package com.java.deliveryapp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.java.deliveryapp.model.product.Product;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@ToString
@Entity
@Table(uniqueConstraints =
@UniqueConstraint(name = "UniqueProductAndUser", columnNames = { "user", "product" }))
public class Favorite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user")
    @JsonIgnore
    User user;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product")
    @JsonIgnore
    Product product;

}